" File settings for markdown filetype

setlocal spell
setlocal textwidth=80
setlocal tabstop=2 shiftwidth=2 softtabstop=2
setlocal formatoptions+=r formatoptions+=o
